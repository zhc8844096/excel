package com.lwp.excel.entity;

import lombok.Data;

/**
 * Copyright (C) @2019 GuangDong Eshore Technology Co. Ltd
 *
 * @author: Administrator
 * @version: 1.0
 * @date: 2019/8/28
 * @time: 14:47
 * @description:
 */
@Data
public class MegedRegionCellResult {

    private boolean merged;

    private int cellMergedLength;

    private int rowMergedLength;

    private int cellIndex;

    private int rowIndex;

    public MegedRegionCellResult(boolean merged) {
        this.merged = merged;
    }

    public MegedRegionCellResult(boolean merged, int cellMergedLength, int rowMergedLength) {
        this.merged = merged;
        this.cellMergedLength = cellMergedLength;
        this.rowMergedLength = rowMergedLength;
    }

    public MegedRegionCellResult(boolean merged, int cellMergedLength, int rowMergedLength, int cellIndex, int rowIndex) {
        this.merged = merged;
        this.cellMergedLength = cellMergedLength;
        this.rowMergedLength = rowMergedLength;
        this.cellIndex = cellIndex;
        this.rowIndex = rowIndex;
    }

    public MegedRegionCellResult() {
    }
}
