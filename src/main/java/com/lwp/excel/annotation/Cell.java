package com.lwp.excel.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Map;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Cell {
    /**
     * 列名（表头）
     * @return
     */
    String value() default "";
    /**
     * 分组
     * @return
     */
    Class<?> [] groups() default {};
    /**
     * 时间类型格式化：
     * 校验注解格式
     * @return
     */
    String format() default "yyyy-MM-dd";
    /**
     * 值为空时，默认值
     * @return
     */
    String defaultValue() default "";
    /**
     * 读取内容转表达式 (如: 0=男,1=女,2=未知)
     * @return
     */
    String readConverterExp() default "";

    /**
     * 行高
     * @return
     */
    short heightInPoints() default 17;

}