package com.lwp.excel.annotation;


import org.apache.poi.ss.usermodel.CellStyle;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 全局样式，局部样式。
 */
@Target({ElementType.TYPE,ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Style {

    /**
     * 水平对齐样式设置
     * ALIGN_GENERAL = 0;//对齐一般
     * ALIGN_LEFT = 1;   //左对齐
     * ALIGN_CENTER = 2; //居中对齐
     * ALIGN_RIGHT = 3;  //右对齐
     * ALIGN_FILL = 4;   //对齐填补
     * ALIGN_JUSTIFY = 5;//两端对齐
     * ALIGN_CENTER_SELECTION = 6;//居中对齐选择

     * @return
     */
    short halign() default CellStyle.ALIGN_LEFT;

    /**
     * 垂直对齐样式设置
     * VERTICAL_TOP = 0; //上端对齐
     * VERTICAL_CENTER = 1;//垂直居中
     * VERTICAL_BOTTOM = 2;//下端对齐
     * VERTICAL_JUSTIFY = 3;//垂直的证明
     * @return
     */
    short valign() default CellStyle.VERTICAL_CENTER;

    /**
     * 边框样式设置：
     * BORDER_NONE = 0; //没有边框
     * BORDER_THIN = 1;
     * BORDER_MEDIUM = 2;
     * BORDER_DASHED = 3;
     * BORDER_HAIR = 4;
     * BORDER_THICK = 5;
     * BORDER_DOUBLE = 6;
     * BORDER_DOTTED = 7;
     * BORDER_MEDIUM_DASHED = 8;
     * BORDER_DASH_DOT = 9;
     * BORDER_MEDIUM_DASH_DOT = 10;
     * BORDER_DASH_DOT_DOT = 11;
     * BORDER_MEDIUM_DASH_DOT_DOT = 12
     * BORDER_SLANTED_DASH_DOT = 13;
     *
     * @return
     */
    short border() default CellStyle.BORDER_NONE;

    /**
     * 设置边框颜色，默认为黑色。
     * @return
     */
    short color() default 8;


    /**
     * 设置内容填充样式
     * short NO_FILL = 0;
     * short SOLID_FOREGROUND = 1;
     * short FINE_DOTS = 2;
     * short ALT_BARS = 3;
     * short SPARSE_DOTS = 4;
     * short THICK_HORZ_BANDS = 5;
     * short THICK_VERT_BANDS = 6;
     * short THICK_BACKWARD_DIAG = 7;
     * short THICK_FORWARD_DIAG = 8;
     * short BIG_SPOTS = 9;
     * short BRICKS = 10;
     * short THIN_HORZ_BANDS = 11;
     * short THIN_VERT_BANDS = 12;
     * short THIN_BACKWARD_DIAG = 13;
     * short THIN_FORWARD_DIAG = 14;
     * short SQUARES = 15;
     * short DIAMONDS = 16;
     * short LESS_DOTS = 17;
     * short LEAST_DOTS = 18;
     * @return
     */
    short fillPattern() default CellStyle.SOLID_FOREGROUND;

    short backgroundColor() default 9;




}
