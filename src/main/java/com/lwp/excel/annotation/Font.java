package com.lwp.excel.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 全局字体设置、内容字体设置：
 * Font分别需要设置字体大小：字体颜色，字体样式
 */
@Target({ElementType.TYPE,ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Font {

    /**
     * 字体大小
     * @return
     */
    short fontHeightInPoints() default 16;

    /**
     * 设置字体颜色
     * @return
     */
    short fontColor() default 0;


    /**
     * 设置字体样式
     * @return
     */
    String fontName() default "宋体";

}
