package com.lwp.excel.exception;

/**
 * Copyright (C) @2019 GuangDong Eshore Technology Co. Ltd
 *
 * @author: Administrator
 * @version: 1.0
 * @date: 2019/8/12
 * @time: 17:00
 * @description:
 */
public class NotHasDataRunTimeException extends RuntimeException {

    private final Integer code = -10002;

    public NotHasDataRunTimeException(String message) {
        super(message);
    }


    public Integer getCode() {
        return code;
    }
}
