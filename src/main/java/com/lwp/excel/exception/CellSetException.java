package com.lwp.excel.exception;

/**
 * Copyright (C) @2019 GuangDong Eshore Technology Co. Ltd
 *
 * @author: Administrator
 * @version: 1.0
 * @date: 2019/8/21
 * @time: 14:40
 * @description:
 */
public class CellSetException extends RuntimeException{
    private final Integer code = -10005;

    public CellSetException(String message) {
        super(message);
    }

    public Integer getCode() {
        return code;
    }
}
