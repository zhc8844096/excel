package com.lwp.excel.exception;

/**
 * Copyright (C) @2019 GuangDong Eshore Technology Co. Ltd
 *
 * @author: Administrator
 * @version: 1.0
 * @date: 2019/8/28
 * @time: 16:14
 * @description:
 */
public class ExcelExportException extends RuntimeException {
    private final Integer code = -10006;

    public ExcelExportException(String message) {
        super(message);
    }

    public Integer getCode() {
        return code;
    }
}
