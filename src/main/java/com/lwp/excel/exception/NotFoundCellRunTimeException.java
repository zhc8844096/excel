package com.lwp.excel.exception;

/**
 * Copyright (C) @2019 GuangDong Eshore Technology Co. Ltd
 *
 * @author: Administrator
 * @version: 1.0
 * @date: 2019/8/12
 * @time: 9:54
 * @description:
 */
public class NotFoundCellRunTimeException extends RuntimeException {

    private final Integer code = -10001;

    public NotFoundCellRunTimeException(String message) {
        super(message);
    }

    public Integer getCode() {
        return code;
    }
}
