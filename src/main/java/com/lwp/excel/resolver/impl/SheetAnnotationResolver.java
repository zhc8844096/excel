package com.lwp.excel.resolver.impl;

import com.lwp.excel.annotation.Sheet;
import com.lwp.excel.resolver.AnnotationResolver;
import com.lwp.excel.util.ExcelUtil;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Copyright (C) @2019 GuangDong Eshore Technology Co. Ltd
 *
 * @author: Administrator
 * @version: 1.0
 * @date: 2019/8/12
 * @time: 18:18
 * @description:
 */
public class SheetAnnotationResolver <T extends Annotation> implements AnnotationResolver<T> {


    /**
     * 解析Sheet标注的类，
     * 并替换字符串，
     * 得到最终的字符串。
     * @param annotation
     * @param target
     * @return
     */
    @Override
    public Object resolve(T annotation, Object target) {
        Sheet sheetAnnotation = (Sheet)annotation;
        //获取 Sheet字符串
        StringBuffer stringBuffer = new StringBuffer(sheetAnnotation.name());
        //判断是否是使用字段作为占位符
        if (sheetAnnotation.value() != null && sheetAnnotation.value().length > 0) {//有占位元素，解析
            String [] fieldStrs = sheetAnnotation.value();
            if (sheetAnnotation.isField()) {//标注了需要按照字段属性替换占位符
                resolver(stringBuffer,target,fieldStrs);
            } else {
                resolver(stringBuffer,fieldStrs);
            }
        }
        return stringBuffer.toString();
    }


    public void resolver(StringBuffer stringBuffer,String[] values) {
        for (int i = 0; i < values.length; i++) {
            if (stringBuffer.indexOf("{", 0) != -1) {
                stringBuffer.replace(stringBuffer.indexOf("{", 0), stringBuffer.indexOf("}", 0)+1, values[i]);
            }
        }
    }

    public Object resolver(StringBuffer stringBuffer,Object target, String[] fieldStrs) {
        Class clazz = target.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field :
                fields) {
            for (int i = 0; i < fieldStrs.length; i++) {
                if (field.getName().equals(fieldStrs[i])) {//是拼接该字段
                    Method method = null;
                    Object val =null;
                    try {
                        if (field.getType().equals("boolean")) {// 基本变量
                            method = clazz.getMethod(ExcelUtil.getBooleanPrefix(field.getName()));
                        } else {
                            method = clazz.getMethod("get" + ExcelUtil.getMethodName(field.getName()));
                        }
                        val = method.invoke(target);
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                    if (val != null ) {//将注解配置中的字段和
                        if (stringBuffer.indexOf("{", 0) != -1) {
                            stringBuffer.replace(stringBuffer.indexOf("{", 0), stringBuffer.indexOf("}", 0)+1, val.toString());
                        }
                    }
                }
            }
        }
        return stringBuffer.toString();
    }

}
