package com.lwp.excel.resolver.impl;

import com.lwp.excel.annotation.Cell;
import com.lwp.excel.annotation.Title;
import lombok.Data;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.util.CellRangeAddress;

import java.util.List;

/**
 * Copyright (C) @2019 GuangDong Eshore Technology Co. Ltd
 *
 * @author: Administrator
 * @version: 1.0
 * @date: 2019/8/13
 * @time: 10:05
 * @description: 解析表格的title
 */
@Data
public class TitleExcelResolver extends SheetExcelResolver {


    public TitleExcelResolver() {
    }

    private String title;

    public TitleExcelResolver(Class<?> group,String title) {
        super(group);
        this.title = title;
    }

    /**
     * 标题，行高占用两行
     *
     * @param sheet
     * @param dataList
     * @param wb
     */
    public void titleResolver(HSSFSheet sheet, List<?> dataList, HSSFWorkbook wb,String[] headers) {
        //int index = this.lastRowIndex(sheet);//获取行索引, 标题的行索引由手动定义。
        Object obj = dataList.get(0);
        Class clazz = obj.getClass();
        Title title = (Title) clazz.getAnnotation(Title.class); //获取Title
        HSSFRow row = sheet.createRow(0);//第0行为Title
        short height = title.heightInPoints();
        row.setHeightInPoints(height);
        HSSFCell cell = row.createCell(0);//创建一列

        HSSFCellStyle style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        if (title != null) {
            cell.setCellValue(this.title);//填充title数据。
        } else {
            cell.setCellValue(title.value());//填充title数据。
        }
        cell.setCellStyle(style);
        int index = this.countParticleCell(clazz,headers);//获取title的宽度（占用几个单元格。）
        sheet.addMergedRegion(new CellRangeAddress(//合并单元格。
                0, // 起始行
                0, // 结束行
                0, // 起始列
                index-1  // 结束列
        ));

    }
}
