package com.lwp.excel.resolver;

/**
 * Copyright (C) @2019 GuangDong Eshore Technology Co. Ltd
 *
 * @author: Administrator
 * @version: 1.0
 * @date: 2019/8/12
 * @time: 13:58
 * @description:
 */
public interface AnnotationResolver <T> {
    Object resolve(T annotation, Object target);
}
