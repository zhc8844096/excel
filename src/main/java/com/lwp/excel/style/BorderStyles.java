package com.lwp.excel.style;

/**
 * 边框样式
 * 边框样式设置：
 * BORDER_NONE = 0; //没有边框
 * BORDER_THIN = 1;
 * BORDER_MEDIUM = 2;
 * BORDER_DASHED = 3;
 * BORDER_HAIR = 4;
 * BORDER_THICK = 5;
 * BORDER_DOUBLE = 6;
 * BORDER_DOTTED = 7;
 * BORDER_MEDIUM_DASHED = 8;
 * BORDER_DASH_DOT = 9;
 * BORDER_MEDIUM_DASH_DOT = 10;
 * BORDER_DASH_DOT_DOT = 11;
 * BORDER_MEDIUM_DASH_DOT_DOT = 12
 * BORDER_SLANTED_DASH_DOT = 13;
 */
public interface BorderStyles {

    short BORDER_NONE = 0; //没有边框
    short BORDER_THIN = 1;
    short BORDER_MEDIUM = 2;
    short BORDER_DASHED = 3;
    short BORDER_HAIR = 4;
    short BORDER_THICK = 5;
    short BORDER_DOUBLE = 6;
    short BORDER_DOTTED = 7;
    short BORDER_MEDIUM_DASHED = 8;
    short BORDER_DASH_DOT = 9;
    short BORDER_MEDIUM_DASH_DOT = 10;
    short BORDER_DASH_DOT_DOT = 11;
    short BORDER_MEDIUM_DASH_DOT_DOT = 12;
    short BORDER_SLANTED_DASH_DOT = 13;


}
