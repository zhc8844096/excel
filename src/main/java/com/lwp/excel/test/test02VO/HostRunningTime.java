package com.lwp.excel.test.test02VO;

import com.lwp.excel.annotation.Cell;
import com.lwp.excel.resolver.ExcelAble;
import lombok.Data;

@Data
public class HostRunningTime implements ExcelAble {
    @Cell("1#")
    private String hostRunningTime1;
    @Cell("2#")
    private String hostRunningTime2;
    @Cell("3#")
    private String hostRunningTime3;
    @Cell("4#")
    private String hostRunningTime4;
}
