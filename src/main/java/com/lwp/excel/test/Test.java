package com.lwp.excel.test;

import com.lwp.excel.util.ExcelUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Copyright (C) @2019 GuangDong Eshore Technology Co. Ltd
 *
 * @author: Administrator
 * @version: 1.0
 * @date: 2019/8/13
 * @time: 11:22
 * @description:
 */
public class Test {


    public static void main(String[] args) throws Exception {

        Long start = System.currentTimeMillis();
        List<Shop> shops = init();
        HSSFWorkbook wb = ExcelUtil.exportExcel(shops);//,
        ExcelUtil.createExcelFile(wb,"/Shop.xls");
        System.out.println(System.currentTimeMillis()-start);
    }

    public static List<Shop> init(){
        List<Shop> shops = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            //System.out.println(getName());
            int income = 9000 + new Random().nextInt(500);
            Shop shop = new Shop(getName(),income);
            /*Manager manager = new Manager();
            shop.setManager(manager);*/
            int size = 6;//3+ new Random().nextInt(3);
            List<Clerk> clerks = new ArrayList<>();
            for (int j = 0; j < size; j++) {
                int clerkInCome = (9000 + new Random().nextInt(500))/size;
                if (income - clerkInCome < 0) {
                    clerkInCome = income;
                }
                Clerk clerk = new Clerk(getName(),18+new Random().nextInt(7)+"",clerkInCome);
                clerks.add(clerk);
            }
            shop.setClerks(clerks);
            shops.add(shop);
        }
        return shops;
    }

    public static String getName(){
        String[] names = {"哎", "雪","恒","永","制","楠","卟","の","斯","欲"};
        StringBuffer stringBuffer = new StringBuffer();
        int size = 2 + new Random().nextInt(2);
        for (int i = 0; i < size; i++) {
            //(int)(Math.random()*10+1)
            int n = new Random().nextInt(10);
            stringBuffer.append(names[n]);
        }
        return stringBuffer.toString();
    }


    public void aaa(){
       /* List a = new ArrayList();
        a.contains()*/
    }
}
