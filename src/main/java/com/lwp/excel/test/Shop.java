package com.lwp.excel.test;

import com.lwp.excel.annotation.*;
import com.lwp.excel.resolver.ExcelAble;
import com.lwp.excel.style.BorderStyles;
import com.lwp.excel.style.ExcelColors;
import com.lwp.excel.style.FillPatternStyles;
import lombok.Data;

import java.util.List;

/**
 * Copyright (C) @2019 GuangDong Eshore Technology Co. Ltd
 *
 * @author: Administrator
 * @version: 1.0
 * @date: 2019/8/9
 * @time: 12:05
 * @description:
 */
@Data
@Sheet(name = "吹雪恒集团" ,password = "lwp")
@Title(value = "吹雪恒集团报表统计",heightInPoints = 30)
public class Shop implements ExcelAble {
    //@Style(border = BorderStyles.BORDER_DASH_DOT,color = ExcelColors.AQUA)
    //@Font(fontHeightInPoints = 12,fontColor = ExcelColors.RED ,fontName = "华文琥珀")
    @Cell(value = "店名sdfasfdsafsafdsafdasfsafasdfd", groups = Clerk.GroupA.class)
    private String name;
    //@Style(backgroundColor = ExcelColors.YELLOW)
    //@Font(fontHeightInPoints = 14,fontColor = ExcelColors.BLUE , fontName = "Bradley Hand ITC")
    @Cell(value = "收入",groups = Clerk.GroupB.class)
    private Integer income;

    //@Cell(value = "店长")
    private Manager manager;

    //@Cell(value = "员工", groups = Clerk.GroupA.class)
    private List<Clerk> clerks ;



    public Shop() {
    }

    public Shop(String name, Integer income) {
        this.name = name;
        this.income = income;
    }
}
